"""
             Projet CyberAttack@IUT'O
        SAe1.01 departement Informatique IUT d'Orleans 2021-2022

    Module jeu.py
"""

import random
import plateau
import protection
import matrice


def creer_jeu(liste_joueur, taille_plateau=5, resistance_serveur=40, resistance_pc=50,
              resistance_protection=2, humain=False, nb_tours_max=-1):
    """Creer un nouveau jeu avec 4 joueurs

    Args:
        liste_joueurs (list): la liste des noms de joueur
        taille_plateau (int, optional): le cote du plateau. Defaults to 5.
        resistance_serveur (int, optional): la resistance du serveur. Defaults to 4.
        resistance_pc (int, optional): la resistance des PC. Defaults to 5.
        resistance_protection (int, optional): la resistance des protections. Defaults to 2.
        humain (bool, optional): indique si le joueur 1 est humain. Defaults to False.
        nb_tours_max (int, optional): indique le nombre de tours de la partie
                                      (-1 pour indiquer pas de limite). Defaults -1.

    Retrns:
        dict: le jeu
    """
    plateau1 = plateau.creer_plateau(1, liste_joueur[0], taille_plateau, resistance_serveur,resistance_pc, nb_points=0)
    plateau2 = plateau.creer_plateau(2, liste_joueur[1], taille_plateau, resistance_serveur,resistance_pc, nb_points=0)
    plateau3 = plateau.creer_plateau(3, liste_joueur[2], taille_plateau, resistance_serveur,resistance_pc, nb_points=0)
    plateau4 = plateau.creer_plateau(4, liste_joueur[3], taille_plateau, resistance_serveur,resistance_pc, nb_points=0)
    liste_plateau = [plateau1, plateau2, plateau3, plateau4]
    
    jeu=dict()
    jeu['liste_joueur']=liste_joueur
    jeu['taille_plateau']=taille_plateau
    jeu['resistance_serveur']=resistance_serveur
    jeu['resistance_pc']= resistance_pc
    jeu['resistance_protection']=resistance_protection
    jeu['humain']=humain
    jeu['nb_tours_max']=nb_tours_max
    jeu['nb_tours']=0
    jeu['liste_plateau']=liste_plateau
    return jeu



def get_taille_plateau(jeu):
    """Retourne la taille des plateau

    Args:
        jeu (dict): un jeu

    Returns:
        int: la taille des plateau
    """
    return jeu["taille_plateau"]


def get_plateau(jeu, id_joueur):
    """ retour le plateau de joueur indenfie par id_joueur

    Args:
        jeu (dict): un jeu
        id_joueur (int): l'identifiant du joueur (entre 1 et 4)

    Returns:
        dict: le plateau du joueur
    """
    for plateau in jeu['liste_plateau']:
        if plateau['id_joueur'] == id_joueur:
            return plateau


def est_fini(jeu):
    """indique si la partie est terminee

    Args:
        jeu (dict): un jeu

    Returns:
        bool: un booleen a True si au moins trois joueur sont elimines ou
              que le nombre de tours max est atteint
    """
    if jeu['nb_tours'] == jeu["nb_tours_max"]:
        return True
    cpt = 0
    for plat in jeu['liste_plateau']:
        if plateau.a_perdu(get_plateau(jeu, plateau.get_id_joueur(plat))):
            cpt +=1
    if cpt == 3:
        return True
    return False


def get_num_tour(jeu):
    """retourne le numero du tour en cours

    Args:
        jeu (dict): un jeu

    Returns:
        int: le numero du tour
    """
    return jeu['nb_tours']


def echange_trojans(jeu):
    """Effectue les echanges de trojans entre les joueurs (des sorties vers les entrees)

    Args:
        jeu (dict): un jeu
    """
    for plat in jeu["liste_plateau"]:
        for (direction,trojans) in plat["trojans_sortants"].items():
            if direction=="H":
                ca=plateau.id_joueur_haut(plat)
                pla=get_plateau(jeu,ca)
                pla["trojans_entrants"]=trojans
            if direction=="G":
                ca=plateau.id_joueur_gauche(plat)
                pla=get_plateau(jeu,ca)
                pla["trojans_entrants"]=trojans
            if direction=="D":
                ca=plateau.id_joueur_droite(plat)
                pla=get_plateau(jeu,ca)
                pla["trojans_entrants"]=trojans


def diriger_trojan(jeu):
    """Applique la protection DONNEES_PERSONNELLES sur les quatre plateaux

    Args:
        jeu (dict): un jeu
    """
    for plat in jeu['liste_plateau']:
        plateau.diriger_trojan(plat)


def phase1(jeu):
    """Effectue les deplacements des trojans sur les 4 plateaux

    Args:
        jeu ((dict): un jeu
    """
    for plat in jeu['liste_plateau']:
        plateau.deplacer_trojan_phase1(plat)


def phase2(jeu):
    """Finalise les deplacements des trojans sur les 4 plateaux.
       cette fonction doit augmenter le numero du tour de jeu de 1

    Args:
        jeu ((dict): un jeu
    """
    for plat in jeu['liste_plateau']:
        plateau.deplacer_trojan_phase2(plat)
    jeu['nb_tours'] += 1


# RECOPIER A PARTIR D'ICI DANS VOTRE FICHIER

def joueur_humain():
    """

    Returns:
        str: une chaine de caracteres indiquant les ordres donnes par la personne
    """
    print("indiquez le direction de votre avatar")
    res = input()

    rep = input(
        "Souhaitez vous (P)oser une protection ou (A)ttaquer les adversaires? (P/A)")
    res += rep
    if rep == 'P':
        print(
            "indiquez le type de protection [O"+str(protection.PAS_DE_PROTECTION)+"]")
        type_protection = input()
        try:
            type_protection = int(type_protection)
        except:
            type_protection = protection.PAS_DE_PROTECTION
        if type_protection != protection.PAS_DE_PROTECTION:
            print("indiquez la position de votre protection")
            ligne = input("numero de la ligne ")
            colonne = input("numero de la colonne")
            try:
                ligne = int(ligne)
                colonne = int(colonne)
            except:
                type_protection = protection.PAS_DE_PROTECTION
        res += str(type_protection)+str(ligne)+str(colonne)
    elif rep == 'A':
        for direction in "GHD":
            print("indiquez le type de virus a envoyer vers "+direction)
            try:
                type_vir = int(input())
            except:
                type_vir = -1
            res += direction+str(type_vir)
    return res


def joueur_aleatoire(le_plateau):
    """produit des ordres aleatoires

    Args:
        le_plateau (dict): un plateau

    Returns:
        str: une chaine de caracteres donnant des ordres compatibles mais aleatoires
        Les ordres sont donnes sous la forme
        d'une chaine de caracteres dont les deux premiers indique le deplacement de l'avatar
        le troisieme caractere est
        soit un A pour une attaque
        soit un P pour une protection
        En cas d'attaque, les caracteres suivants sont GxHyDz ou
                    x y et z sont des chiffres entre 0 et 4 indiquant le numero de la
                             ligne ou de la colonne ou sera envoye le trojan
        En cas de pose d'une protection les caractere suivants seront trois chiffre tlc ou
                    t est le type de la protection
                    l la ligne ou poser la protection
                    c la colonne ou poser la protection
    """
    # choix du deplacement de l'avatar
    res = random.choice(list(plateau.DIRECTIONS_AVATAR))
    taille = plateau.get_taille(le_plateau)
    # choix entre poser une protection ou attaquer les adversaires
    if random.randint(0, 1) == 0:
        ligne = random.randint(0, taille-1)
        colonne = random.randint(0, taille-1)
        ind_protect = random.randint(0, protection.PAS_DE_PROTECTION-1)
        if ligne != taille//2 or colonne != taille//2:
            res += 'P'+str(ind_protect)+str(ligne)+str(colonne)
    else:  # on attaque les adversaires
        res += 'A'
        les_voisins = ['G', 'H', 'D']
        for direct in les_voisins:
            res += direct+str(random.randint(0, 4))
    return res


def actions_joueur(jeu):
    """Recolte et execute les actions choisies par chacun des joueurs

    Args:
        jeu (dict): un jeu
    """
    for id_joueur in range(1, 5):
        le_plateau = get_plateau(jeu, id_joueur)
        if plateau.a_perdu(le_plateau):
            continue
        if id_joueur == 1 and est_humain(jeu):
            ordres = joueur_humain()
        else:
            ordres = joueur_aleatoire(le_plateau)

        plateau.executer_ordres(le_plateau, ordres)
    echange_trojans(jeu)


def actions_joueur_ext(jeu, ordres):
    """Permet de faire jouer chaque joueur un tour de jeu

    Args:
        jeu (dict): le jeu sur lequel on joue
        ordres (dict): un dictionnaire dont les cles sont les numeros de joueur 
                       et les valeurs les str donnant les ordres de chaque joueur
    """
    for id_joueur in range(1, 5):
        le_plateau = get_plateau(jeu, id_joueur)
        if plateau.a_perdu(le_plateau):
            continue
        plateau.executer_ordres(le_plateau, ordres[id_joueur])
    echange_trojans(jeu)


def jeu_2_str(jeu, sep="\n||----||\n"):
    """Transforme un jeu en str pour le transfert via le reseau

    Args:
        jeu (dict): le jeu a transformer
        sep (str, optional): ce qui separe deux plateaux. Defaults to "\n||----||\n".

    Returns:
        str: la chaine de caracteres qui encode le jeu
    """
    type_joueur = 'O'
    if est_humain(jeu):
        type_joueur = 'H'
    res = str(get_num_tour(jeu))+';'+str(get_nb_tours_max(jeu))+';'+type_joueur
    for i in range(1, 5):
        res += sep+plateau.plateau_2_str(get_plateau(jeu, i))
    return res


def creer_jeu_from_str(jeu_str, sep="\n||----||\n"):
    """creer un jeu a partir d'une chaine de caracteres

    Args:
        jeu_str (str): la chaine de caracteres qui encode le jeu
        sep (str, optional): le separateur de plateau. Defaults to "\n||----||\n".

    Returns:
        dict: le jeu code dans la chaine de caracteres
    """
    plateaux = jeu_str.split(sep)
    nb_tours, nb_tours_max, type_joueur = plateaux[0].split(";")
    nb_tours = int(nb_tours)
    nb_tours_max = int(nb_tours_max)
    humain = type_joueur == 'H'
    liste_plateaux = []
    for ind in range(1, len(plateaux)):
        liste_plateaux.append(plateau.creer_plateau_from_str(plateaux[ind]))
    return creer_jeu_en_cours(nb_tours, nb_tours_max, humain, liste_plateaux)


def sauver_jeu(jeu, nom_fic):
    """sauvegarde un jeu dans un fichier

    Args:
        jeu (dict): le jeu a sauvegarder
        nom_fic (str): le nom du fichier ou sauvegarder le jeu
    """
    with open(nom_fic, "w") as fic:
        fic.write(jeu_2_str(jeu))


def charger_jeu(nom_fic):
    """creer un jeu a partir d'une sauvegarde

    Args:
        nom_fic (str): le nom du fichier

    Returns:
        dict: le jeu lu dans le fichier
    """
    with open(nom_fic) as fic:
        chaine = fic.read()
        return creer_jeu_from_str(chaine)


# fonctions additionnelles sur le jeu
def set_nom_joueur(jeu, id_joueur, nom_joueur):
    """change le nom du joueur numero id_joueur

    Args:
        jeu (dict): le jeu
        id_joueur (int): un nombre entre 1 et 4 indiquant le joueur que l'on veut modifier
        nom_joueur (str): le nom du joueur
    """
    jeu[id_joueur] = nom_joueur


def est_humain(jeu):
    """Indique si le joueur 1 est humain ou non

    Args:
        jeu (dict): le jeu

    Returns:
        bool: True si le joueur 1 est humain
    """
    return jeu["humain"]


def get_nb_tours_max(jeu):
    """Retourne le nombre de tours maximum pour la partie

    Args:
        jeu (dict): le jeu

    Returns:
        int: le nombre de tours maximum du jeu 
    """
    return jeu["nb_tours_max"]


def creer_jeu_en_cours(num_tours, nb_tours_max, humain, liste_plateaux):
    """cree un jeu a partir des informations donnes en parametres. liste_plateaux
       donne la liste des plateaux dans l'ordre des joueurs (liste_plateaux[0] est le plateau du joueur 1 etc.)


    Args:
        num_tours (int): le numero du tour
        nb_tours_max (int): le nombre de tours maximum
        humain (bool): True si le joueur 1 est un humain
        liste_plateaux (list): la liste des 4 plateaux des 4 joueurs

    Returns:

    """
    jeu=dict()
    
    jeu['taille_plateau']=5
    jeu['resistance_serveur']=40
    jeu['resistance_pc']= 50
    jeu['resistance_protection']=2
    jeu['humain']=humain
    jeu['nb_tours_max']=nb_tours_max
    jeu['nb_tours']=num_tours
    jeu['liste_plateau']=liste_plateaux
    
    liste_joueur = []
    for plat in liste_plateaux:
        liste_joueur.append(plateau.get_nom_joueur(plat))

    jeu['liste_joueur']=liste_joueur
    return jeu

def joueur_ia(jeu, id_joueur):
    """calcule les action du joueur id_joueur en fonction de l'etat du jeu

    Args:
        jeu (dict): le jeu
        id_joueur (int): un nombre entre 1 et 4 indiquant quel joueur doit jouer
    Returns:
        str: la chaine de caracteres donnant les ordres choisis par le joueur
    """
    # a titre d'exemple
    le_plateau = get_plateau(jeu, id_joueur)
    return joueur_aleatoire(le_plateau)
