"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module joueur.py
Module de gestion des joueurs
"""


def creer_joueur(id_joueur, nom_joueur, nb_points=0):
    """créer un nouveau joueur

    Args:
        id_joueur (int): l'identifiant du joueur (un entier de 1 à 4)
        nom_joueur (str): le nom du joueur
        nb_points (int, optional): le nombre de points du joueur. Defaults to 0.

    Returns:
        dict: le joueur
    """
    joueur = dict()
    joueur['id_joueur'] = id_joueur
    joueur['nom_joueur'] = nom_joueur
    joueur['nb_points'] = nb_points
    return joueur


def get_id(joueur):
    """retourne l'identifiant du joueur

    Args:
        joueur (dict): un joueur

    Returns:
        int: l'identifiant du joueur
    """
    return joueur['id_joueur']


def get_nom(joueur):
    """retourne le nom du joueur

    Args:
        joueur (dict): un joueur

    Returns:
        str: nom du joueur
    """
    return joueur['nom_joueur']

def get_points(joueur):
    """retourne le nombre de points du joueur

    Args:
        joueur (dict): un joueur

    Returns:
        int: le nombre de points du joueur
    """
    return joueur['nb_points']


def ajouter_points(joueur, points):
    """ajoute des points au joueur

    Args:
        joueur (dict): un joueur
        points (int): le nombre de points à ajouter

    Returns:
        int: le nombre de points du joueur
    """
    joueur['nb_points']+=points
    return joueur['nb_points']



def id_joueur_droite(joueur):
    """retourne l'identifiant du joueur à droite d'un joueur

    Args:
        joueur (dict): un joueur

    Returns:
        int: l'identifiant du joueur de droite
    """
    pos_joueur_d = joueur['id_joueur'] + 1
    if pos_joueur_d > 4:
        pos_joueur_d -= 4
    return pos_joueur_d


def id_joueur_gauche(joueur):
    """retourne l'identifiant du joueur à gauche d'un joueur

    Args:
        joueur (dict): un joueur

    Returns:
        int: l'identifiant du joueur de gauche
    """
    pos_joueur_g = joueur['id_joueur'] + 3
    if pos_joueur_g > 4:
        pos_joueur_g -= 4
    return pos_joueur_g

def id_joueur_haut(joueur):
    """retourne l'identifiant du joueur au dessus d'un joueur

    Args:
        joueur (dict): un joueur

    Returns:
        int: l'identifiant du joueur du haut
    """
    pos_joueur_h = joueur['id_joueur'] + 2
    if pos_joueur_h > 4:
        pos_joueur_h -= 4
    return pos_joueur_h

def set_nom(joueur, nom_joueur):
    """permet de changer le nom du joueur

    Args:
        joueur (dict): un joueur
        nom_joueur (str): un nom de joueur
    """
    joueur['nom_joueur'] = nom_joueur