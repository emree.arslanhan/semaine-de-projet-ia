"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module matrice.py
    Ce module de gestion des matrices
"""


def creer_matrice(nb_lig, nb_col, val_defaut=None):
    """créer une matrice contenant nb_lig lignes et nb_col colonnes avec
       pour valeur par défaut val_defaut

    Args:
        nb_lig (int): un entier strictement positif
        nb_col (int): un entier strictement positif
        val_defaut (Any, optional): La valeur par défaut des éléments de la matrice.
                                    Defaults to None.
    Returns:
        dict: la matrice
    """
    matrice = dict()
    matrice['nb_lig'] = nb_lig
    matrice['nb_col'] = nb_col
    matrice['liste_val'] = [val_defaut] * (nb_lig * nb_col)
    return matrice

def get_nb_lignes(matrice):
    """retourne le nombre de lignes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de lignes de la matrice
    """
    return matrice['nb_lig']


def get_nb_colonnes(matrice):
    """retourne le nombre de colonnes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de colonnes de la matrice
    """
    return matrice['nb_col']


def get_val(matrice, lig, col):
    """retourne la valeur en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)

    Returns:
        Any: la valeur en lig, col de la matrice
    """
    return matrice['liste_val'][lig * matrice['nb_col'] + col]

def set_val(matrice, lig, col, val):
    """stocke la valeur val en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)
        val (Any): la valeur à stocker
    """
    matrice['liste_val'][lig * matrice['nb_col'] + col] = val


def max_matrice(matrice, interdits=None):
    """retourne la liste des coordonnées des cases contenant la valeur la plus grande de la matrice
        Ces case ne doivent pas être parmi les interdits.

    Args:
        matrice (dict): une matrice
        interdits (set): un ensemble de tuples (ligne,colonne) de case interdites. Defaults to None

    Returns:
        list: la liste des coordonnées de cases de valeur maximale dans la matrice (hors cases interdites)
    """
    liste = []
    lig = 0
    col = 0
    val_max = None

    for elem in matrice['liste_val']:
        if val_max == None or val_max < elem:
            if interdits == None or (lig, col) not in interdits:
                val_max = elem

        if col < matrice['nb_col']-1:
            col +=1
        else:
            col = 0
            lig += 1
    lig = 0
    col = 0
    for val in matrice['liste_val']:
        if val == val_max:
            if interdits == None or (lig, col) not in interdits:
                liste.append((lig, col))
        
        if col < matrice['nb_col']-1:
            col +=1
        else:
            col = 0
            lig += 1
    return liste


DICO_DIR = {(-1, -1): 'HD', (-1, 0): 'HH', (-1, 1): 'HG', (0, -1): 'GG',
            (0, 1): 'DD', (1, -1): 'BG', (1, 0): 'BB', (1, 1): 'BD',}

def direction_max_voisin(matrice, ligne, colonne):
    """retourne la liste des directions qui permettent d'aller vers la case voisine de 
       la case (ligne,colonne) la plus grande. Le résultat doit aussi contenir la 
       direction qui permet de se rapprocher du milieu de la matrice
       si ligne,colonne n'est pas le milieu de la matrice

    Args:
        matrice (dict): une matrice
        ligne (int): le numéro de la ligne de la case considérée
        colonne (int): le numéro de la colonne de la case considérée

    Returns:
        str: deux lettres indiquant la direction DD -> droite , HD -> Haut Droite,
                                                 HH -> Haut, HG -> Haut gauche,
                                                 GG -> Gauche, BG -> Bas Gauche, BB -> Bas
    """
    liste = []
    millieu = (matrice["nb_lig"]//2,matrice["nb_col"]//2)
    max = None

    for lig,col in DICO_DIR.keys():
        if max==None or max<=get_val(matrice,ligne+lig,colonne+col):
            if max==get_val(matrice,ligne+lig,colonne+col):
                liste.append(DICO_DIR[(lig,col)])
            else:
                max=get_val(matrice,ligne+lig,colonne+col)
                liste=[DICO_DIR[(lig,col)]]
                
    for lig,col in DICO_DIR.keys():
        if (ligne+lig,colonne+col)==millieu:
            liste.append(DICO_DIR[(lig,col)])

    return liste
